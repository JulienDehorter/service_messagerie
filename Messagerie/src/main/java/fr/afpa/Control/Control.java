package fr.afpa.Control;
import fr.afpa.Display.*;
import java.util.ArrayList;
import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.afpa.Display.FrameNouveauMessage;
import fr.afpa.Hibernate.HibernateUtils;
import fr.afpa.beans.Utilisateur;

public class Control {

	public static boolean ControlChampVide(StringBuilder sb) {
		if (sb.length()==1 ){
			return true;

		}
		else {return false;}
	}

	public static boolean ControlLoginExist(StringBuilder sb) {
		boolean verif = false;
		int index = sb.indexOf("/");
		String login = sb.substring(0, index);
		String mdp =sb.substring(index+1);
		
		Session s = null;
		  s = HibernateUtils.getSession();
		Query q = s.getNamedQuery("VerificationPersonne");
		q.setString("login", login);
		q.setString("mdp", mdp);
   		ArrayList<Utilisateur> al  = (ArrayList<Utilisateur>) q.getResultList();
   		if(al.size()>0) {
   			verif = true;
   		}
		

		return verif;
	}

	public boolean ControlAuth(String str) {

		return false;
	}

	public boolean ControlTelephone(String str) {
		return false;
	}

	public static boolean ControlEmail(String str) {
		boolean verif = false;
		Session s = null;
		  s = HibernateUtils.getSession();
		Query q = s.getNamedQuery("VerificationEmail");
		FrameNouveauMessage fnm = new FrameNouveauMessage();
		q.setParameter("mail",str) ;
   		ArrayList<Utilisateur> al  = (ArrayList<Utilisateur>) q.getResultList();
   		if(al.size()>0) {
   			verif = true;
   		}
				
				
		return verif;
	}
}
