package fr.afpa.beans;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor


@Entity
//@NamedQuery(name="checkEmail", query="select email from utilisateur")
@NamedQuery(name="RecuperationPersonne", query="select u from Utilisateur u where login = :login")
@NamedQuery(name="VerificationEmail", query="select u from Utilisateur u WHERE u.mail= :mail")
@NamedQuery(name="VerificationPersonne", query="select u from Utilisateur u WHERE u.login= :login AND u.mdp= :mdp")
@Table(name="Utilisateur", uniqueConstraints=
@UniqueConstraint(columnNames= {"pk_utilisateur_id","Mail","Login"}))

public class Utilisateur {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="pk_utilisateur_id")
	private int idUtilisateur;
	@Column(name="nom")
	private String nom;
	@Column(name="prenom")
	private String prenom;
	@Column(name="mail")
	private String mail;	
	@Column(name="telephone")
	private String telephone;
	@Column(name="login")
	private String login;	
	@Column(name="mdp")
	private String mdp;
	@OneToMany(mappedBy="idExpediteur")
	private List <Mail> listeEmailRecu;
	@OneToMany(mappedBy="id_Destinataire")
	private List <Mail> listeEmailEnvoye;	



}
