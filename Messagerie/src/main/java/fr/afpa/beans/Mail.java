package fr.afpa.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor



@Entity
@NamedQuery(name="findEmailReçu", query="Select m from Mail m where m.idExpediteur = :id")
@Table(name="Mail", uniqueConstraints=
@UniqueConstraint(columnNames= {"pk_id_Email"}))

public class Mail {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="pk_id_Email")
	private int idEmail;

	@Column(name="Objet")
	private String Objet;
	@Column(name="Corps")
	private String Corps;
	@Column(name="Lu")
	private Boolean Ouvert;
	@ManyToOne(
			cascade= {CascadeType.MERGE}
			)
	@JoinColumn(name="fk_id_Expediteur")
	private Utilisateur idExpediteur;
	@ManyToOne(
			cascade= {CascadeType.MERGE}
			)
	@JoinColumn(name="fk_id_Destinataire")
	private Utilisateur id_Destinataire;




}
