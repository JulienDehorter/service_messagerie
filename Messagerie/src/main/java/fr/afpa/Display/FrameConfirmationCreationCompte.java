package fr.afpa.Display;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 * Hello world!
 *
 */
public class FrameConfirmationCreationCompte implements ActionListener
{
    static JFrame fenetreConfirmationCreationCompte = new JFrame();

    public static void FrameConfirmationCreationCompte()
    {
    	
        fenetreConfirmationCreationCompte.setTitle("CDA Messagerie");
        
        BorderLayout blFCC = new BorderLayout();        
        fenetreConfirmationCreationCompte.setLayout(blFCC);
        
        JPanel pConfirmationConnexion = new JPanel();
        
        Border blackline = BorderFactory.createTitledBorder(" Création de Compte :");   
        pConfirmationConnexion.setBorder(blackline);
        pConfirmationConnexion.setLayout(new GridLayout(3,2));
        pConfirmationConnexion.add(new JLabel("        "));       
        pConfirmationConnexion.add(new JLabel("        "));       
        JLabel messageConfirmation = new JLabel("Votre compte a été créé avec succès.");
        pConfirmationConnexion.add(messageConfirmation);
        JButton btnRetourAuth = new JButton("Retour à la page d'authentification >");
        btnRetourAuth.addActionListener(new FrameConfirmationCreationCompte());
        pConfirmationConnexion.add(btnRetourAuth);
        pConfirmationConnexion.add(new JLabel("        "));       
        pConfirmationConnexion.add(new JLabel("        "));       
        fenetreConfirmationCreationCompte.add(pConfirmationConnexion,BorderLayout.CENTER);

        JPanel pConfirmationConnexion2 = new JPanel ();
        pConfirmationConnexion2.setLayout(new GridLayout(5,1));
        pConfirmationConnexion2.add(Box.createHorizontalGlue());       
        pConfirmationConnexion2.add(Box.createHorizontalGlue());       
        pConfirmationConnexion2.add(Box.createHorizontalGlue());       
        pConfirmationConnexion2.add(Box.createHorizontalGlue()); 
        pConfirmationConnexion2.add(Box.createHorizontalGlue());
        pConfirmationConnexion2.add(Box.createHorizontalGlue());
        pConfirmationConnexion2.add(Box.createHorizontalGlue());
        pConfirmationConnexion2.add(Box.createHorizontalGlue());
        pConfirmationConnexion2.add(Box.createHorizontalGlue()); 
        pConfirmationConnexion2.add(new JLabel("        "));       
       fenetreConfirmationCreationCompte.add(pConfirmationConnexion2,BorderLayout.SOUTH);
       
       JPanel pConfirmationConnexionEast = new JPanel ();
       pConfirmationConnexionEast.setLayout(new GridLayout(5,2));
       pConfirmationConnexionEast.add(Box.createHorizontalGlue());     
       pConfirmationConnexionEast.add(Box.createHorizontalGlue());   
       pConfirmationConnexionEast.add(Box.createHorizontalGlue());   
       pConfirmationConnexionEast.add(Box.createHorizontalGlue());   
       pConfirmationConnexionEast.add(Box.createHorizontalGlue());   
       pConfirmationConnexionEast.add(new JLabel("           "));       
       pConfirmationConnexionEast.add(Box.createHorizontalGlue());   
       pConfirmationConnexionEast.add(Box.createHorizontalGlue());   
       pConfirmationConnexionEast.add(Box.createHorizontalGlue());   
       pConfirmationConnexionEast.add(Box.createHorizontalGlue());   
       pConfirmationConnexionEast.add(Box.createHorizontalGlue());         
      fenetreConfirmationCreationCompte.add(pConfirmationConnexionEast,BorderLayout.EAST);
       
       JPanel pConfirmationConnexionNorth = new JPanel();   
       pConfirmationConnexionNorth.setLayout(new GridLayout(3,1));
       pConfirmationConnexionNorth.add(new JLabel("                                "));
       pConfirmationConnexionNorth.add(Box.createHorizontalGlue());
       pConfirmationConnexionNorth.add(Box.createHorizontalGlue());
       fenetreConfirmationCreationCompte.add(pConfirmationConnexionNorth, BorderLayout.NORTH);


       
       JPanel pConfirmationConnexionWest = new JPanel();   
       pConfirmationConnexionWest.setLayout(new GridLayout(1,1));
       pConfirmationConnexionWest.add(new JLabel("                                "));
       fenetreConfirmationCreationCompte.add(pConfirmationConnexionWest, BorderLayout.WEST);

       


        
        fenetreConfirmationCreationCompte.setBounds(480, 270, 1000, 200);
        fenetreConfirmationCreationCompte.setSize(700, 400);
        fenetreConfirmationCreationCompte.setPreferredSize(fenetreConfirmationCreationCompte.getSize());
        fenetreConfirmationCreationCompte.pack();
        fenetreConfirmationCreationCompte.setVisible(true);
        
        
        
     
        
    }

	@Override
	public void actionPerformed(ActionEvent arg0) {

		fenetreConfirmationCreationCompte.setVisible(false);
		FrameLogin fl = new FrameLogin();
		fl.frameLogin();
	}
}
