package fr.afpa.Display;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.afpa.Hibernate.HibernateUtils;
import fr.afpa.beans.Mail;
import fr.afpa.beans.Utilisateur;


public class FrameAccueil implements ActionListener {
	static JFrame fenetreAccueil = new JFrame();
	static int compteur = 0;


	public void FrameAccueil() {
		
		FrameLogin.fenetreLogin.setVisible(false);
		// Création fenetreAccueil, bouton, textfields, etc...
		fenetreAccueil.setLayout(new BorderLayout());
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		p2.setLayout(new GridLayout(2, 4));
		JPanel p3 = new JPanel();
		p3.setPreferredSize(new Dimension(80, 25));
		JPanel p4 = new JPanel();
		p4.setPreferredSize(new Dimension(80, 100));
		JPanel p5 = new JPanel();
		p5.setPreferredSize(new Dimension(80, 65));
		JPanel p6 = new JPanel();
		JPanel p7 = new JPanel();

		// Création des boutons de menu
		JMenuBar menu_bar1 = new JMenuBar();
		JMenu m1 = new JMenu("Fichier");
		JMenu m2 = new JMenu("Help");

		JMenuItem mi1 = new JMenuItem("Nouveau message");
		mi1.setName("NouveauMessage");
		mi1.addActionListener(new FrameAccueil());
		JMenuItem mi2 = new JMenuItem("Deconnexion");
		mi2.addActionListener(new FrameAccueil());
		mi2.setName("Deconnexion");
		JMenuItem mi3 = new JMenuItem("En cas de problème, brisez la vitre et contactez votre administrateur");
		m1.add(mi1);
		m1.add(mi2);

		m2.add(mi3);

		menu_bar1.add(m1);
		menu_bar1.add(m2);

		// Zone de texte qu'il faudra setter pour afficher les contenus des différents
		// tabs

		p1.setLayout(new GridLayout(3, 3));
		JLabel taEmpty1 = new JLabel();
		taEmpty1.setText("nom");
		JLabel taEmpty12 = new JLabel();
		taEmpty12.setText("prenom");
		JLabel taEmpty13 = new JLabel();
		taEmpty13.setText("age");
		JLabel taEmpty14 = new JLabel();
		taEmpty14.setText("oui");
		JLabel taEmpty15 = new JLabel();
		taEmpty15.setText("non");
		JLabel taEmpty16 = new JLabel();
		taEmpty16.setText("maybe");
		JLabel taEmpty17 = new JLabel();
		taEmpty17.setText("what");
		JLabel taEmpty18 = new JLabel();
		taEmpty18.setText("hi");
		JLabel taEmpty19 = new JLabel();
		taEmpty19.setText("grrr");

		p2.setLayout(new GridLayout(3, 3));
		JLabel taEmpty2 = new JLabel();
		taEmpty1.setText("nom");
		JLabel taEmpty22 = new JLabel();
		taEmpty12.setText("prenom");
		JLabel taEmpty23 = new JLabel();
		taEmpty13.setText("age");
		JLabel taEmpty24 = new JLabel();
		taEmpty14.setText("oui");
		JLabel taEmpty25 = new JLabel();
		taEmpty15.setText("non");
		JLabel taEmpty26 = new JLabel();
		taEmpty16.setText("maybe");
		JLabel taEmpty27 = new JLabel();
		taEmpty17.setText("what");
		JLabel taEmpty28 = new JLabel();
		taEmpty18.setText("hi");
		JLabel taEmpty29 = new JLabel();
		taEmpty19.setText("grrr");
		JLabel taEmpty3 = new JLabel();

		// Ajout des différents tabs
		JTabbedPane tp1 = new JTabbedPane();

		// Implémentation des différents labels du panneau "Boite de reception"
		tp1.addTab("Boite de réception", p1);
		p1.add(taEmpty1);
		p1.add(taEmpty12);
		p1.add(taEmpty13);
		p1.add(taEmpty14);
		p1.add(taEmpty15);
		p1.add(taEmpty16);
		p1.add(taEmpty17);
		p1.add(taEmpty18);
		p1.add(taEmpty19);

//		// Ajout service message envoyés en cours de développement -IE
//		// Implémentation des différents labels du panneau "Messages Envoyés"
//		JComboBox liste1 = new JComboBox();
//		ArrayList <Mail> listeMail = new ArrayList();
//		listeMail = mailEnvoyes(FrameLogin.utilisateur);
//		String [] tab = new String[listeMail.size()*3];
//		int i =0;
//		for(Mail m : listeMail) {
//			tab[i++] = m.getIdExpediteur().getNom();
//			tab[i+1] = m.getObjet();
//			tab[i+2] = m.getCorps();
//			i+=3;
//		}
		
		
		tp1.addTab("Messages envoyés", p6);


		tp1.addTab("Recherche de mail", p7);
		p7.add(taEmpty3);

		// Affichage du titre
		String bonjour = "bonjour "+FrameLogin.utilisateur.getNom()+" "+FrameLogin.utilisateur.getPrenom();
		JLabel l1 = new JLabel(bonjour);
		l1.setHorizontalAlignment(l1.CENTER);
		l1.setFont(new Font("Verdana", Font.PLAIN, 18));

		p2.add(l1);

		// Assemblage final
		fenetreAccueil.setJMenuBar(menu_bar1);
		fenetreAccueil.add(tp1, BorderLayout.CENTER);
		fenetreAccueil.add(p2, BorderLayout.NORTH);
		fenetreAccueil.add(p3, BorderLayout.EAST);
		fenetreAccueil.add(p4, BorderLayout.WEST);
		fenetreAccueil.add(p5, BorderLayout.SOUTH);

		fenetreAccueil.setTitle("CDA Messagerie");
		fenetreAccueil.pack();
		fenetreAccueil.setBounds(480, 270, 1000, 200);
		fenetreAccueil.setSize(700, 400);
		fenetreAccueil.setVisible(true);

	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
	JMenuItem mi = ((JMenuItem)arg0.getSource());
		
		if (mi.getName().equals("NouveauMessage")) {
			fenetreAccueil.setVisible(false);
			FrameNouveauMessage fnm = new FrameNouveauMessage();
			fnm.FrameNouveauMessage();
		}
		else if (mi.getName().equals("Deconnexion")) {
			fenetreAccueil.setVisible(false);
			FrameLogin fl = new FrameLogin();
			fl.frameLogin();
			
			
		}
	}
	
//	 // Recherche des mails envoyés en cours de développement
//	public ArrayList<Mail> mailEnvoyes(Utilisateur str) {
//		ArrayList<Mail> listeMail = new ArrayList<Mail>();
//		Session s = null;
//		  s = HibernateUtils.getSession();
//		Query q = s.getNamedQuery("VerificationEmail");
//		q.setParameter("id", str);
// 		List lm  = (List) q.getResultList();
//		
//		
//		return listeMail;
//	}



}
