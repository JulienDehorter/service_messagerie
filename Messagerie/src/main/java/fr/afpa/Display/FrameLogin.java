package fr.afpa.Display;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.persistence.Query;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.hibernate.Session;

import fr.afpa.Control.Control;
import fr.afpa.Hibernate.HibernateUtils;
import fr.afpa.beans.Mail;
import fr.afpa.beans.Utilisateur;

public class FrameLogin implements ActionListener {

	private static JLabel messageErreur = new JLabel();
	// private static JLabel messageErreur2 = new JLabel("Les champs ne doivent pas
	// être vide !");
	private static JTextField t1;
	private static JTextField t2;
	private JButton b1;
	private JButton b2;
	private Utilisateur u;
	static JFrame fenetreLogin = new JFrame();
	static int compteur = 0;
	static Utilisateur utilisateur;

	public void frameLogin() {
		
		fenetreLogin.setVisible(true);
		
		if (compteur == 0) {
			// Création fenetre, bouton, textfields, etc...
			fenetreLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			JButton b1 = new JButton("Création de compte");
			JButton b2 = new JButton("Connexion");
			b1.addActionListener(new FrameLogin());
			b2.addActionListener(new FrameLogin());

			t1 = new JTextField();
			t2 = new JTextField();
			JTextField t3 = new JTextField("");
			t3.setVisible(false);
			JTextField t4 = new JTextField("");
			t4.setVisible(false);
			messageErreur.setVisible(false);
			JTextField t6 = new JTextField();
			t6.setVisible(false);

			JPanel p1 = new JPanel();
			JPanel p2 = new JPanel();
			GridLayout grid = new GridLayout(5, 2);
			grid.setHgap(15);
			grid.setVgap(5);
			p2.setLayout(grid);

			JPanel p3 = new JPanel();
			JPanel p4 = new JPanel();
			JPanel p5 = new JPanel();

			JLabel l1 = new JLabel("CDA 20156 - Messagerie");
			l1.setHorizontalAlignment(l1.CENTER);
			l1.setPreferredSize(new Dimension(80, 60));
			JLabel l2 = new JLabel("Login");
			l2.setFont(new Font("Verdana", Font.PLAIN, 18));
			JLabel l3 = new JLabel("Mot de passe");
			l3.setFont(new Font("Verdana", Font.PLAIN, 18));
			JLabel empty = new JLabel("");

			p1.setLayout(new BorderLayout());

			// Assemblages des différentes parties dans la Frame fenetre
			p1.add(empty, BorderLayout.NORTH);
			p1.add(l1, BorderLayout.CENTER);
			p1.add(empty, BorderLayout.SOUTH);
			l1.setFont(new Font("Verdana", Font.PLAIN, 25));

			p2.add(messageErreur);
			p2.add(t6);
			p2.add(l2);
			p2.add(t1);
			p2.add(l3);
			p2.add(t2);
			p2.add(t3);
			p2.add(t4);
			p2.add(b1);
			p2.add(b2);

			p3.add(empty);
			p3.setPreferredSize(new Dimension(80, 60));

			p4.add(empty);
			p4.setPreferredSize(new Dimension(80, 60));

			p5.add(empty);
			p5.setPreferredSize(new Dimension(80, 60));

			fenetreLogin.setLayout(new BorderLayout());

			fenetreLogin.add(p1, BorderLayout.NORTH);
			fenetreLogin.add(p2, BorderLayout.CENTER);
			fenetreLogin.add(p3, BorderLayout.SOUTH);
			fenetreLogin.add(p4, BorderLayout.WEST);
			fenetreLogin.add(p5, BorderLayout.EAST);

			fenetreLogin.setTitle("CDA Messagerie");
			fenetreLogin.pack();
			fenetreLogin.setVisible(true);
			fenetreLogin.setBounds(480, 270, 1000, 200);
			fenetreLogin.setSize(700, 400);
			fenetreLogin.setResizable(false);
			compteur++;
		}
		else {
			t1.setText("");
			t2.setText("");
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		StringBuilder sb = new StringBuilder();
		sb.append(t1.getText());
		sb.append("/");
		sb.append(t2.getText());
		JButton b = ((JButton) e.getSource());

		if (b.getText().equals("Création de compte")) {
			fenetreLogin.setVisible(false);
			FrameCreationCompte fcc = new FrameCreationCompte();
			fcc.FrameCreationCompte();
		} 
		else if (b.getText().equals("Connexion")) {

			if (Control.ControlChampVide(sb)) {
				messageErreur.setText("Les champs ne doivent pas être vides !");
				messageErreur.setVisible(true);
			}

			else if (!Control.ControlLoginExist(sb)) {
				messageErreur.setText("Login/mdp incorrect !");

				messageErreur.setVisible(true);
			}

			// FrameLogin.frameLogin();

			else {				
				
				
				
				this.fenetreLogin.setVisible(false);
						   		Utilisateur u = null;
				Session s = null;
				  s = HibernateUtils.getSession();
				Query q = s.getNamedQuery("RecuperationPersonne");
				q.setParameter("login", t1.getText());
		   		ArrayList<Utilisateur> al  = (ArrayList<Utilisateur>) q.getResultList();
		   		for (Utilisateur p : al) {
		   			utilisateur = p;
		   			ArrayList<Mail> alr = new ArrayList<Mail>();
		   			ArrayList<Mail> ale = new ArrayList<Mail>();
		   			utilisateur.setListeEmailEnvoye(ale);
		   			utilisateur.setListeEmailRecu(new ArrayList<Mail>(alr));
		   			break;
		   		}
				FrameAccueil fa = new FrameAccueil();
				fa.FrameAccueil();
			}
		}
	}

}
