package fr.afpa.Display;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import fr.afpa.Control.Control;
import fr.afpa.Hibernate.HibernateUtils;
import fr.afpa.beans.Mail;
import fr.afpa.beans.Utilisateur;
import lombok.Getter;

@Getter
/**
 * Hello world!
 *
 */
public class FrameNouveauMessage implements ActionListener
{
	static JFrame fenetreNouveauMessage = new JFrame();
	public static JTextField destinataire;
	private static JTextArea contenuMessage;
	private static JTextField objetMail;
	private static JLabel destInc = new JLabel("Le destinataire de votre email n'existe pas.");
	private static int compteur =0;
	
    public void FrameNouveauMessage() {
    	FrameAccueil.fenetreAccueil.setVisible(false);
    	if (compteur ==0) {
        fenetreNouveauMessage.setTitle("CDA Messagerie");
        BorderLayout blFCC = new BorderLayout();        
        fenetreNouveauMessage.setLayout(blFCC);
		JMenuBar menu_bar1 = new JMenuBar();
		JMenu m1 = new JMenu("Fichier");
		JMenu m2 = new JMenu("Help");

		JMenuItem mi2 = new JMenuItem("Deconnexion");
		mi2.setName("Deconnexion");
		mi2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
					fenetreNouveauMessage.setVisible(false);
					FrameLogin fl = new FrameLogin();
					fl.frameLogin();			
			}
		});
		JMenuItem mi3 = new JMenuItem("En cas de problème, brisez la vitre et contactez votre administrateur");
		m1.add(mi2);

		m2.add(mi3);

		menu_bar1.add(m1);
		menu_bar1.add(m2);
		fenetreNouveauMessage.setJMenuBar(menu_bar1);
        
        
        
        
        JPanel pNouveauMessageCenter = new JPanel();
        JPanel pNouveauMessageCenterNorth = new JPanel();
        pNouveauMessageCenterNorth.setLayout(new GridLayout(2,2));
        objetMail = new JTextField(20);
        pNouveauMessageCenterNorth.add(new JLabel("     À :"));
        pNouveauMessageCenterNorth.add(destinataire);
        pNouveauMessageCenterNorth.add(new JLabel("Objet : "));
        pNouveauMessageCenterNorth.add(objetMail);
        Border blackline = BorderFactory.createTitledBorder(" Nouveau message :");   
        pNouveauMessageCenter.setBorder(blackline);
        pNouveauMessageCenter.add(pNouveauMessageCenterNorth,BorderLayout.NORTH);
        
        
        JPanel pNouveauMessageCenterCenter = new JPanel();
        pNouveauMessageCenterCenter.setLayout(new BorderLayout(1,1));
        destinataire = new JTextField(20);
        contenuMessage = new JTextArea();
        contenuMessage.setSize(450,100);
        contenuMessage.setPreferredSize(contenuMessage.getSize());
        pNouveauMessageCenterCenter.add(contenuMessage);
        pNouveauMessageCenter.add(pNouveauMessageCenterCenter,BorderLayout.CENTER);

        
        JPanel pNouveauMessageCenterSouth = new JPanel();
        pNouveauMessageCenterSouth.setLayout(new GridLayout(1,6));
        JButton envoyer = new JButton("Envoyer");
        envoyer.setName("Envoyer");
        envoyer.addActionListener(new FrameNouveauMessage());
        pNouveauMessageCenterSouth.add(envoyer);
        pNouveauMessageCenterSouth.add(Box.createHorizontalGlue());
        pNouveauMessageCenterSouth.add(Box.createHorizontalGlue());
        pNouveauMessageCenterSouth.add(Box.createHorizontalGlue());
        pNouveauMessageCenterSouth.add(Box.createHorizontalGlue());
        pNouveauMessageCenterSouth.add(Box.createHorizontalGlue());
        pNouveauMessageCenter.add(pNouveauMessageCenterSouth,BorderLayout.SOUTH);
        fenetreNouveauMessage.add(pNouveauMessageCenter,BorderLayout.CENTER);


        JPanel pNouveauMessage2 = new JPanel ();
        pNouveauMessage2.setLayout(new GridLayout(5,1));
        pNouveauMessage2.add(Box.createHorizontalGlue());       
        pNouveauMessage2.add(Box.createHorizontalGlue());       
        pNouveauMessage2.add(Box.createHorizontalGlue());       
        pNouveauMessage2.add(Box.createHorizontalGlue()); 
        pNouveauMessage2.add(Box.createHorizontalGlue());
        pNouveauMessage2.add(Box.createHorizontalGlue());
        pNouveauMessage2.add(Box.createHorizontalGlue());
        pNouveauMessage2.add(Box.createHorizontalGlue());
        pNouveauMessage2.add(Box.createHorizontalGlue()); 
        pNouveauMessage2.add(new JLabel("        "));       
       fenetreNouveauMessage.add(pNouveauMessage2,BorderLayout.SOUTH);
       
       JPanel pNouveauMessageEast = new JPanel ();
       pNouveauMessageEast.setLayout(new GridLayout(5,2));
       pNouveauMessageEast.add(Box.createHorizontalGlue());     
       pNouveauMessageEast.add(Box.createHorizontalGlue());   
       pNouveauMessageEast.add(Box.createHorizontalGlue());   
       pNouveauMessageEast.add(Box.createHorizontalGlue());   
       pNouveauMessageEast.add(Box.createHorizontalGlue());   
       pNouveauMessageEast.add(new JLabel("         "));       
       pNouveauMessageEast.add(Box.createHorizontalGlue());   
       pNouveauMessageEast.add(Box.createHorizontalGlue());   
       pNouveauMessageEast.add(Box.createHorizontalGlue());   
       pNouveauMessageEast.add(Box.createHorizontalGlue());   
       pNouveauMessageEast.add(Box.createHorizontalGlue());         
      fenetreNouveauMessage.add(pNouveauMessageEast,BorderLayout.EAST);
       
       JPanel pNouveauMessageNorth = new JPanel();   
       pNouveauMessageNorth.setLayout(new GridLayout(1,3));
       destInc.setVisible(false);
       pNouveauMessageNorth.add(new JLabel("                                "));
       pNouveauMessageNorth.add(destInc);
       pNouveauMessageNorth.add(new JLabel("                                "));
       fenetreNouveauMessage.add(pNouveauMessageNorth, BorderLayout.NORTH);


       
       JPanel pNouveauMessageWest = new JPanel();   
       pNouveauMessageWest.setLayout(new GridLayout(1,1));
       pNouveauMessageWest.add(new JLabel("              "));
       fenetreNouveauMessage.add(pNouveauMessageWest, BorderLayout.WEST);

       
       
        

        
        fenetreNouveauMessage.setBounds(480, 270, 1000, 200);
        fenetreNouveauMessage.setSize(700, 400);
        fenetreNouveauMessage.setPreferredSize(fenetreNouveauMessage.getSize());
        fenetreNouveauMessage.pack();
        fenetreNouveauMessage.setVisible(true);
    	}
    	else {
			contenuMessage.setText("");
			destinataire.setText("");
			objetMail.setText(""); 			
   	}
        
        
     
        
    }

	@Override
	public void actionPerformed(ActionEvent arg0) {
		boolean verif = Control.ControlEmail(destinataire.getText());
		if (verif== true) {
						Mail m = new Mail();
						Utilisateur exp = Recuperationid(FrameLogin.utilisateur.getMail());
						Utilisateur dest = Recuperationid(destinataire.getText());
						m.setIdExpediteur(exp);
			m.setId_Destinataire(dest);
			m.setCorps(contenuMessage.getText());
			m.setObjet(objetMail.getText());
			Session s = null;
			  s = HibernateUtils.getSession();
				Transaction tx = s.beginTransaction();
			
			
			s.save(m);
			tx.commit();
			
			fenetreNouveauMessage.setVisible(false);
			FrameConfirmationEnvoiMessage fcem = new FrameConfirmationEnvoiMessage();
			fcem.FrameConfirmationEnvoiMessage();
		}
		else {
			destInc.setVisible(true);
		}
		
	}
	
	public Utilisateur Recuperationid(String str) {
		Utilisateur id = null;
		Session s = null;
		  s = HibernateUtils.getSession();
		Query q = s.getNamedQuery("VerificationEmail");
		q.setParameter("mail", str);
 		ArrayList<Utilisateur> al  = (ArrayList<Utilisateur>) q.getResultList();
 		for (Utilisateur u : al){
 			id = u;
 		}
		s.clear();
		s.close();
		return id;
	}



}
