package fr.afpa.Display;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FrameBoiteReception {
	
	public static void FrameBoiteReception() {
		

		// Création fenetre, bouton, textfields, etc...
		JFrame fenetre = new JFrame();
		fenetre.setLayout(new BorderLayout());
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(2, 2));
		p1.setPreferredSize(new Dimension(80, 60));
		JPanel p2 = new JPanel();
		p2.setPreferredSize(new Dimension(60, 40));
		p2.setBorder(BorderFactory.createLineBorder(Color.black));
		JPanel p3 = new JPanel();
		p3.setPreferredSize(new Dimension(80, 25));
		JPanel p4 = new JPanel();
		p4.setPreferredSize(new Dimension(20,20 ));
		JPanel p5 = new JPanel();
		p5.setPreferredSize(new Dimension(80, 100));
		
		JTextField t1 = new JTextField();
		JTextField t2 = new JTextField();

		JButton b1 = new JButton("Retour à la page d'accueil");
		b1.setHorizontalAlignment(b1.LEFT);
		b1.setPreferredSize(new Dimension(20, 30));
		

		// b1.setVerticalAlignment(b1.BOTTOM);

		// Zone de texte qu'il faudra setter pour afficher les contenus des différents
		// tabs
		JLabel taEmpty1 = new JLabel();	
		JLabel taEmpty2 = new JLabel();	
		JLabel taEmpty3 = new JLabel();			
		taEmpty1.setLayout(new GridLayout(1, 3));		

		// Affichage du titre
		JLabel l1 = new JLabel("  Bonjour"+"");
		l1.setHorizontalAlignment(l1.LEFT);
		// l1.setVerticalAlignment(l1.TOP);
		l1.setFont(new Font("Verdana", Font.PLAIN, 18));

		// Assemblage final

		p1.add(l1);
		p1.add(t1);
		t1.setVisible(false);
		p1.add(b1);
		p1.add(t2);
		t2.setVisible(false);

		p2.add(taEmpty1);
		p2.add(taEmpty2);
		p2.add(taEmpty3);

		fenetre.add(p1, BorderLayout.NORTH);
		fenetre.add(p2, BorderLayout.CENTER);
		fenetre.add(p3, BorderLayout.EAST);
		fenetre.add(p4, BorderLayout.WEST);
		fenetre.add(p5, BorderLayout.SOUTH);

		fenetre.setTitle("CDA Messagerie");
		fenetre.pack();
		fenetre.setBounds(480, 270, 1000, 200);
		fenetre.setSize(700, 400);
		fenetre.setVisible(true);
	}

}
