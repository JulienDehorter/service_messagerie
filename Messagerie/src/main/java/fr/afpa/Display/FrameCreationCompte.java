package fr.afpa.Display;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import fr.afpa.Hibernate.HibernateUtils;
import fr.afpa.beans.Utilisateur;

/**
 * Hello world!
 *
 */
public class FrameCreationCompte implements ActionListener
{
	
   static JFrame fenetreCreationCompte = new JFrame();
   private static JTextField tfNom;
   private static JTextField tfPrenom;
   private static JTextField tfEmail;
   private static JTextField tfTelephone;
   private static JTextField tfLogin;
   private static JTextField tfMDP;
   static JLabel jl = new JLabel("Merci de ne pas laisser de cases vide.");

   
    public void FrameCreationCompte()
    {
    	   JLabel lNom = new JLabel("Nom : ");    
    	   tfNom = new JTextField(20);
    	   JLabel lPrenom = new JLabel("Prénom : ");
    	   tfPrenom = new JTextField(20);
    	   JLabel lEmail = new JLabel("Mail : ");
    	   tfEmail = new JTextField(35);
    	   JLabel lTelephone = new JLabel("Num téléphone : ");
    	   tfTelephone = new JTextField(10);        
    	   JLabel lLogin = new JLabel("Login : ");
    	   tfLogin = new JTextField(20);
    	   JLabel lMDP = new JLabel("Mot de passe : ");
    	   tfMDP = new JTextField(20);
        fenetreCreationCompte.setTitle("CDA Messagerie");
        
        BorderLayout blFCC = new BorderLayout();        
        fenetreCreationCompte.setLayout(blFCC);
        
        JPanel pCreationCompte = new JPanel();
        GridLayout gl = new GridLayout(6,3);
        
        pCreationCompte.setLayout(gl);
        
        // Instanciation des labels et des textfields qui composeront le label 1

      
        Border blackline = BorderFactory.createTitledBorder(" Création de Compte :");   
        pCreationCompte.setBorder(blackline);
        pCreationCompte.add(lNom);
        pCreationCompte.add(tfNom); 
        pCreationCompte.add(lPrenom);
        pCreationCompte.add(tfPrenom);  
        pCreationCompte.add(lEmail);
        pCreationCompte.add(tfEmail);  
        pCreationCompte.add(lTelephone);
        pCreationCompte.add(tfTelephone); 
        pCreationCompte.add(lLogin);
        pCreationCompte.add(tfLogin); 
        pCreationCompte.add(lMDP);
        pCreationCompte.add(tfMDP);
        JButton btn = new JButton("Valider");
        btn.setName("button1");
        btn.addActionListener(new FrameCreationCompte());
        pCreationCompte.add(tfMDP);
        fenetreCreationCompte.add(pCreationCompte,BorderLayout.CENTER);
        
        
        JPanel pCreationCompte2 = new JPanel();
        JButton btn1 = new JButton("Valider");
        btn.setName("button1");
        pCreationCompte2.setLayout(new GridLayout(2,7));
        pCreationCompte2.add(new JLabel("        "));       
        pCreationCompte2.add(new JLabel("        "));       
        pCreationCompte2.add(new JLabel("        "));       
        pCreationCompte2.add(new JLabel("                ")); 
        pCreationCompte2.add(btn);   
        pCreationCompte2.add(new JLabel("                "));
        pCreationCompte2.add(new JLabel("        "));       
        pCreationCompte2.add(new JLabel("        "));       
        pCreationCompte2.add(new JLabel("        "));       
        pCreationCompte2.add(new JLabel("        "));       
        pCreationCompte2.add(new JLabel("        "));       
       fenetreCreationCompte.add(pCreationCompte2,BorderLayout.SOUTH);
      
       
       JPanel pCreationCompteNorth = new JPanel();   
       pCreationCompteNorth.setLayout(new GridLayout(1,3));
       jl.setVisible(false);
       pCreationCompteNorth.add(new JLabel("                  "));
       pCreationCompteNorth.add(jl);
       pCreationCompteNorth.add(new JLabel("                  "));


       
       JPanel pCreationCompteWest = new JPanel();   
       pCreationCompteWest.setLayout(new GridLayout(1,1));
       pCreationCompteWest.add(new JLabel("                                "));

       JPanel pCreationCompteEast = new JPanel();
       pCreationCompteEast.setLayout(new GridLayout(1,1));
       pCreationCompteEast.add(new JLabel("                                "));

       
       fenetreCreationCompte.add(pCreationCompteNorth, BorderLayout.NORTH);
       fenetreCreationCompte.add(pCreationCompteWest, BorderLayout.WEST);
       fenetreCreationCompte.add(pCreationCompteEast, BorderLayout.EAST);
       

        

        
        fenetreCreationCompte.setBounds(480, 270, 1000, 200);
        fenetreCreationCompte.setSize(700, 400);
        fenetreCreationCompte.setPreferredSize(fenetreCreationCompte.getSize());
        fenetreCreationCompte.pack();
        fenetreCreationCompte.setVisible(true);
        
        
        
     
        
    }

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String str = tfNom.getText();
		if (tfNom.getText().isEmpty() || tfPrenom.getText().isEmpty() || tfEmail.getText().isEmpty() || tfTelephone.getText().isEmpty() || tfLogin.getText().isEmpty() || tfMDP.getText().isEmpty() ) {
			jl.setVisible(true);
			System.out.println(str+"zrgrzgb");		
		}
	else {
		Session s = null;
		  s = HibernateUtils.getSession();

			Transaction tx = s.beginTransaction();
			Utilisateur u = new Utilisateur();
			u.setNom(tfNom.getText());
			u.setPrenom(tfPrenom.getText());
			u.setMail(tfEmail.getText());
			u.setTelephone(tfTelephone.getText());
			u.setLogin(tfLogin.getText());
			u.setMdp(tfMDP.getText());
			s.save(u);
			tx.commit();		

		fenetreCreationCompte.setVisible(false);
		FrameConfirmationCreationCompte fccc = new FrameConfirmationCreationCompte();
		fccc.FrameConfirmationCreationCompte();
		
		}
}
	
}
