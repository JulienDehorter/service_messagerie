package fr.afpa.Display;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import fr.afpa.beans.Utilisateur;

public class FrameConfirmationEnvoiMessage implements ActionListener{
	static JFrame fenetre = new JFrame();

	
	public static void FrameConfirmationEnvoiMessage() {


		fenetre.setLayout(new BorderLayout());
		JPanel p1 = new JPanel();
		Border linebor = BorderFactory.createTitledBorder("Envoi de message");
		Border color = BorderFactory.createLineBorder(new Color(000000), 2);

		p1.setBorder(color);
		p1.setBorder(linebor);

		JPanel p2 = new JPanel();
		p2.setPreferredSize(new Dimension(80, 65));
		JPanel p3 = new JPanel();
		p3.setPreferredSize(new Dimension(80, 25));
		JPanel p4 = new JPanel();
		p4.setPreferredSize(new Dimension(80, 100));
		JPanel p5 = new JPanel();
		p5.setPreferredSize(new Dimension(80, 65));
		JLabel l1 = new JLabel("Message envoyé");
		JButton b1 = new JButton("Retour à la page d'accueil");
		b1.addActionListener(new FrameConfirmationEnvoiMessage());

		p1.add(l1);
		p1.add(b1);

		fenetre.add(p1, BorderLayout.CENTER);
		fenetre.add(p2, BorderLayout.NORTH);
		fenetre.add(p3, BorderLayout.EAST);
		fenetre.add(p4, BorderLayout.SOUTH);
		fenetre.add(p5, BorderLayout.WEST);

		fenetre.setTitle("CDA Messagerie");
		fenetre.pack();
		fenetre.setVisible(true);
		fenetre.setBounds(480, 270, 1000, 200);
		fenetre.setSize(700, 400);

	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
			fenetre.setVisible(false);
			FrameAccueil fa = new FrameAccueil();
			Utilisateur u = new Utilisateur();
			fa.FrameAccueil();
	}
}